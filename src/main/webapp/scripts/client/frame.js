var isMaximal = undefined;
var isWide = false;
var statusLastId = -1;
var currentMode = "";

function onSearchSubmit(obj){
	$.address.value($(obj).attr("action") + "/" + encodeURI($("input[name='keyword']", obj).val()) + "/");
	return false;
}

var rankHTML = "";
function getRankView() {
	if (!rankHTML) {
		$.ajax({
			url : MXB.url_suffix + "/movie_rank.ajax",
			type : 'POST',
			success: function (e) {
				rankHTML = e;
				$("#movie-rank-view").html(e);
			}
		});
	}
	return rankHTML;
}

function headerScrollScan(){

	if ($(".ui-widget-overlay").length > 0) return;
	if ($(this).scrollTop() > 125 && !$("#header").hasClass("lock")) {
		$("#header").addClass("lock");
	}
	if ($(this).scrollTop() <= 125 && $("#header").hasClass("lock")) {
		$("#header").removeClass("lock");

	}
}

function getMenuSummray() {
	$("*[data-summary-url]").each(function () {
		$(this).load($(this).attr("data-summary-url"));
	});
}

shortcut.add("F5",function() { MXB.changeURI(); });
MXB.bind("page_ready", function (jqXHR){

	$(".global_nb li").removeClass("selected");
	$(".global_nb li.menu0" + getHeader(jqXHR, "mxb-menu_status")).addClass("selected");

	var keyword = getHeader(jqXHR, "mxb-search-keyword");
	if (keyword) {
		keyword = decodeURI(keyword);
		keyword = keyword.replace("+", " ");
		$("#column").selectbox("detach").val(getHeader(jqXHR, "mxb-search-colum")).selectbox("attach");
		$("#keyword").val(keyword).prev().hide();
	}
	else {
		$("#keyword").val("").prev().show();
	}

	$("#header").removeClass("lock");
	
	$("select").selectbox();
	$("#movie-rank-view").html(getRankView());
	
	$(window).resize();
});

$(window).resize(function (){

	if (getCookie("status-folding") != "NO") {
		$("#status_command li[data-action='fold']").addClass("on");
		isMaximal = false;
		$("body").removeClass("maximal");
		$("body").addClass("minimal");
		$("#player").css({ "height" : "44px" });
	}
	else {
		if (parseInt($("body").width()) >= 1260 && (isMaximal == false || isMaximal == undefined)) {
			isMaximal = true;
			$("#player").css({ "width" : "240px", "height" : "100%"  });
			$("body").removeClass("minimal");
			$("body").addClass("maximal");
		}
		else if (parseInt($("body").width()) < 1260 && (isMaximal == true || isMaximal == undefined)) {
			isMaximal = false;
			$("#player").css({ "height" : "44px" });
			$("body").removeClass("maximal");
			$("body").addClass("minimal");
		}
	}
	
	if (parseInt($("body").width()) >= 1670 && isWide == false) {
		isWide = true;
		$("#player li:has(.fullsize)").removeClass("disabled");
		$("#player li:has(.basicsize)").removeClass("disabled");
		
		if (getCookie("view-wide") != "NO") {
			$("body").addClass("wide");
			$("body").trigger("mxb.screen_resize", "wide");
			$("#player li:has(.fullsize)").addClass("selected");
			$("#player li:has(.basicsize)").removeClass("selected");
		}
		else {
			$("body").removeClass("wide");
			$("body").trigger("mxb.screen_resize", "basic");
			$("#player li:has(.fullsize)").removeClass("selected");
			$("#player li:has(.basicsize)").addClass("selected");
		}
	}
	else if (parseInt($("body").width()) < 1670 && isWide == true) {
		isWide = false;
		$("body").removeClass("wide");
		$("body").trigger("mxb.screen_resize", "basic");
		$("#player li:has(.fullsize)").addClass("disabled");
		$("#player li:has(.basicsize)").addClass("disabled");
	}
	else if (parseInt($("body").width()) < 1670) {
		$("#player li:has(.fullsize)").addClass("disabled");
		$("#player li:has(.basicsize)").addClass("disabled");
	}
});

$(document).bind("scroll", headerScrollScan);
$(document).ready(function (){

	$("#active_menu").css({ height: "0px" });

	MXB.loop.addQue("menu_summray", getMenuSummray, 30);
	getMenuSummray();
	
	$(".active-menu li").mouseenter(function () {
		$("#active_menu").clearQueue().animate({ height: "340px" }, 200);
		var l = $(this).offset().left + ($(this).outerWidth() / 2) - 10;
		$("#active_menu .arrow").show().clearQueue().animate({ marginLeft: l + "px" }, 150);
		var i = $(this).attr("data-sidx");
		var x = $("#active_menu .rail div[data-to-sidx='" + i + "']").position().left;
		$("#active_menu .rail").clearQueue().animate({"left": (0 - x) + "px"}, 100);
	});
	
	$(".active-menu-off").mouseenter(function () {
		$("#active_menu").clearQueue().animate({ height: "0px" }, 200);
		$("#active_menu .arrow").hide();
	});

	$("#header").mouseleave(function () {
		$("#active_menu").clearQueue().animate({ height: "0px" }, 200);
		$("#active_menu .arrow").hide();
	});
	
	
	$("#keyword").focus(function (){ $(this).prev().hide(); });
	$("#keyword").focusout(function (){ if(!$(this).val()) $(this).prev().show(); });
	if($("#keyword").val()) $("#keyword").prev().hide();
	$(window).resize();
	
	$("#player").mouseenter(function (){
		if(isMaximal) return;
		$(this).clearQueue();
		
		$(this).animate({ "width" : "240px", "height" : ($("body").height() - 20) + "px" }, 200);
	});
	$("#player").mouseleave(function (){
		if(isMaximal) return;
		$(this).clearQueue();
		$(this).animate({ "height" : "44px" }, 200);
	});

	$("#history_container").mouseenter(function (){
		$(this).attr("data-in-mouse", "Y");
	});
	$("#history_container").mouseleave(function (){
		$(this).attr("data-in-mouse", "N");
	});

	$("#status_command li[data-action]").click(function (){
		if (getCookie("status-folding") != "NO") {
			setCookie("status-folding", "NO");
			$(this).removeClass("on");
		}
		else {
			setCookie("status-folding", "YES");
			$(this).addClass("on");
		}
		$(window).resize();
	});
	
	$("#status_command li input[data-view-val]").click(function (){
		if (parseInt($("body").width()) < 1670) return;
		setCookie("view-wide", $(this).attr("data-view-val"));
		if ($(this).attr("data-view-val") == "YES") {
			$("body").addClass("wide");
			$("body").trigger("mxb.screen_resize", "wide");
			$("#player li:has(.fullsize)").addClass("selected");
			$("#player li:has(.basicsize)").removeClass("selected");
		}
		else {
			$("body").removeClass("wide");
			$("body").trigger("mxb.screen_resize", "basic");
			$("#player li:has(.fullsize)").removeClass("selected");
			$("#player li:has(.basicsize)").addClass("selected");
		}
	});

	$("#player").show();
});
