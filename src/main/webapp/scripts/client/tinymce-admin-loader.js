
tinymce.init({
	'selector' : '.tinymce-editor',

	'width' : '100%',
	'height' : '500',
	
	'theme' : 'advanced',
	'skin' : 'extjs',
	'schema' : 'html5',

	'plugins' : 'autolink, lists, pagebreak, style, layer, table, advhr, advimage, advlink, emotions, iespell, inlinepopups, contextmenu, paste, directionality, noneditable, visualchars, nonbreaking, xhtmlxtras, template',
	'theme_advanced_buttons1' : 'code,|,bold,italic,underline,strikethrough,fontselect,fontsizeselect,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,forecolor,backcolor,outdent,indent,blockquote',
	'theme_advanced_buttons2' : 'tablecontrols,|,hr,sub,sup,|,charmap,iespell,|,link,unlink',
	'toolbar_items_size' : 'small',

	'theme_advanced_toolbar_location' : "top",
	'theme_advanced_toolbar_align' : "left",
	'theme_advanced_statusbar_location' : "none",
	
	mode : "textareas",
	
	'remove_script_host' : false,
	'convert_urls' : false,

	'convert_newlines_to_brs' : true,
	'content_css' : url_suffix + '/stylesheets/editor.css'
});