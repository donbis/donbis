
$(document).ready(function () {
	$("#tabhost a[data-idx]").click(function () {
		$("#tabhost li").removeClass("selected");
		$("li", this).addClass("selected");
		$(".tab_item").hide();
		$("#" + $(this).attr("data-idx")).show();
	})
});