$(function() {
	if ($('#msg_viewframe').size() > 0) {
		$('#msg_viewframe').bind("load", frame_loaded());
		$('#msg_viewframe').trigger("load");
		setTimeout(function() {
			iframe_resize();
		}, 1000);
	}

	$("#post-form").bind("submit", function(e) {
		e.preventDefault();
		return false;
	});

	$("#submit").bind(
			"click",
			function(e) {
				var frm = $("#post-form");

				$("textarea", frm.get(0)).each(
						function() {
							$('#' + $(this).attr("id")).html(
									tinymce.get($(this).attr("id"))
											.getContent());

						});
				if ($("#title").val() == "") {
					alert("제목을 입력해주세요.");
					return;
				}

				$.ajax({
					url : frm.attr("action"),
					type : frm.attr("method"),
					data : frm.serialize(),
					dataType : "json",
					/*
					 * error: function(request, status, error) { alert("글이 등록되지
					 * 않았습니다.\n잠시 후 다시 등록해 주세요." + error);
					 * document.location.reload(); },
					 */
					success : function(data) {
						if (!data.success) {
							alert("글이 등록되지 않았습니다.\n > " + data.message);
							return;
						}
						window.location.href = data.redirect_url;
					}
				});

				e.preventDefault();
				return false;
			});
	$("#submit").bind("mouseover", function() {
		$(this).css('cursor', 'pointer');
	});

	$('#fileupload').fileupload(
			{

				url : url_suffix + "/app_img/create.json",
				dataType : 'json',
				acceptFileTypes : /(\.|\/)(gif|jpe?g|png|bmp)$/i,
				disableImageResize : true,
				done : function(e, data) {
					var tpl = $("#file_template").html();
					tpl = tpl.replace(/\{id\}/gi, data.result.files[0].id);
					$(tpl).click(
							function() {
								var content = tinymce.get('content')
										.getContent({
											format : 'raw'
										});
								tinymce.get('content').setContent(
										content + '<img src="' + url_suffix
												+ '/app_img/get/'
												+ $(this).attr("data-id")
												+ '" />');
							}).appendTo(file_list);

					$('#fileupload').val("");
				}
			}).prop('disabled', !$.support.fileInput).parent().addClass(
			$.support.fileInput ? undefined : 'disabled');

	$("input[type='file']").bind('change', function() {
		$(this).parents("form").submit();
		$(this).val("");
	});
});
function frame_loaded() {
	$('#msg_viewframe').get(0).contentWindow.document.write($(
			"#message_content").val());
	$('#msg_viewframe').contents().find("a").bind("click", function(e) {
		window.open($(this).attr("href"));
		e.preventDefault();
		return false;
	});
	iframe_resize();
	// $("img, link",
	// $(this).contents().find('body').get(0)).load(iframe_resize());
}

function iframe_resize() {
	var doc = $('#msg_viewframe').contents();
	var frm = $('#msg_viewframe').get(0);
	if (doc.size() > 0 && frm) {
		$(frm).css({
			"height" : doc.outerHeight() + "px"
		});
	}
}