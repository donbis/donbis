$(function() {
	$("#delete").css("cursor", "pointer").click(function() {
		var result = confirm("정말 삭제하시겠습니까?");
		if (result) {
			var frm = $("#delete");
			$.ajax({
				url : frm.attr("url"),
				dataType : "json",
				success : function(data) {
					if (!data.success) {
						alert("오류 발생.\n > " + data.message);
						return;
					}
					window.location.href = data.redirect_url;
				}
			});
		} else {
			location.reload();
		}
	});
});