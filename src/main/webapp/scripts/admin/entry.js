$(function(){
	$(".btnWin").bind("click",function (e) {
  		var $this = $(this);
  		var active = $(this).hasClass("active");
  		var dataId = $(this).parent().parent().children("td:first-child").children("input").val();
  		
  		$.ajax({
			url : context_path+"/admin/event/entry/win/"+dataId,
			type : "post",
			data : "status="+!active,
			dataType : "json",
			error: function(request, status, error) { 
				alert("시스템 장애로 변경되지 않았습니다.\n잠시후 다시 시도해 주세요.");
				//document.location.reload();
			},
			success : function (data) {
				if (!data.success) {
					alert("당첨자 설정이 완료되지 않았습니다.\n잠시후 다시 시도해 주세요.");
					return;
				}else{
					if(data.result) $this.addClass("active");
					else $this.removeClass("active");
				}
			}
		});
		e.preventDefault();
		return false;
  	});
});