var upload_url = "";
$(document).ready(function () {
	$('#fileupload').fileupload({
	    url: url_suffix + "/app_img/create.json",
	    dataType: 'json',
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png|bmp)$/i,
        disableImageResize: true,
	    done: function (e, data) {
	    	var tpl = $("#file_template").html();
	    	tpl = tpl.replace(/\{id\}/gi, data.result.files[0].id);
	    	$(tpl).click(function () { 
	    		//alert($(this).attr("data-id"));
	    		var content = tinymce.get('mainContent').getContent({format : 'raw'});
	    		tinymce.get('mainContent').setContent(content+'<img src="'+url_suffix+'/app_img/get/'+$(this).attr("data-id")+'" />');
	    	}).appendTo(file_list);
	    	
	    	$('#fileupload').val("");
	    }
	})
	.prop('disabled', !$.support.fileInput)
	.parent()
		.addClass($.support.fileInput ? undefined : 'disabled');
});