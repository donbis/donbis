$(function(){
	$("#abBg").bind("click",function(e){
		$("#abBg").hide();
		$(".popCon").hide();
		$("#popMemblock .blockFrm .userId").val("");
	});
	$("#popMemblock > .btnClose").bind("click",function(e){
		$("#abBg").hide();
		$("#popMemblock").hide();
		$("#popMemblock .blockFrm .userId").val("");
	});
	
	$(".btnBlock").bind("click",function(e){
		var $this = $(this);
  		var dataId = $(this).parent().parent().children("td:first-child").children("input").val();
  		
		$("#abBg").show();
		$("#popMemblock").show();
		$("#popMemblock .blockFrm .userId").val(dataId);
	});
});