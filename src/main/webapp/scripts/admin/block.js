$(function(){
	$(".btnReset").bind("click",function (e) {
  		var $this = $(this);
  		var active = $(this).hasClass("bg-success");
  		var dataId = $(this).parent().parent().children("td:first-child").children("input").val();

  		$.ajax({
			url : context_path+"/admin/event/entry/block/"+dataId,
			type : "post",
			data : "status="+!active,
			dataType : "json",
			error: function(request, status, error) { 
				alert("시스템 장애로 변경되지 않았습니다.\n잠시후 다시 시도해 주세요.");
				//document.location.reload();
			},
			success : function (data) {
				if (!data.success) {
					alert("블락설정이 완료되지 않았습니다.\n잠시후 다시 시도해 주세요.");
					return;
				}else{
					if(!data.result){
						 $this.removeClass("bg-success");
						 $this.addClass("bg-danger");
						 $this.text("Block");
					}else{
						 $this.removeClass("bg-danger");
						 $this.addClass("bg-success");
						 $this.text("Reset");
					}
				}
			}
		});
		e.preventDefault();
		return false;
  	});
});