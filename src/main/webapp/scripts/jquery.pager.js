if ( typeof Object.create !== "function" ) {
	Object.create = function( obj ) {
		function F() {};
		F.prototype = obj;
		return new F();
	};
}
$(function(){
	var mxbPager = {
		reinit: function () {
			this.init( this.target, this.options, this.el);
		},

		init: function (target, options, el){
			var base = this;
			base.options = $.extend({}, $.fn.mxbPager.options, options);
			
			if (base.options.target) this.dispose();
			base.options.page = 1;
			base.options.param = {};
			base.options.target = target;

			if (base.options.target.find(".paging_list .pager").length > 0) {
				base.options.target.find(".paging_list").data("hasPager", true);
				base.options.target.find(".paging_list > *").each(function (key, item){
					if (item != target.find(".paging_list .pager")[0]) $(item).remove();
				});
			}

			base.options.target.find(".pager #more").bind("click", this.pressEvent);
			base.options.target.find(".pager").show();

			if (base.options.target.attr("data-scrollable") == "true") {console.log("scrollable");
				var $T = base;
				$(window).bind("scroll", function () {
					var ct = $T.options.target.find(".pager").offset().top - $(this).scrollTop();
					if ($(this).innerHeight() > ct && !$T.options.isLoading) $T.pressEvent();
				});
			}

			if (base.options.target.attr("data-init-param")) {
				var $T = base.options;
				var params = $T.target.attr("data-init-param").split('&');
				for(var param in params)
				{
					var val = params[param].split('=');
					$T.param[val[0]] = val[1];
				}
			}
			base.append();
		},

		dispose: function (){
			if (!base.options.target) return;
			$(window).unbind("scroll");
			if (base.options.target.attr("data-scrollable") == "true")
				$(window).unbind("scroll");
			base.options.target.find(".pager #more").unbind("click");
			delete base.options.target;
			delete base.options.param;
		},

		putParam: function (key, value){
			var $T = this;

			this.page = 1;
			if((typeof key).toString().toLowerCase() == "object")
			{
				$.each(key, function (key_2, value_2){
					$T.param[key_2] = value_2;
				});
			}
			else
				this.param[key] = value;

			base.options.target.find(".paging_list").html("");
			base.options.target.find(".pager").show();
			base.options.append();
		},

		getParam: function (key){
			return base.options.param[key];
		},

		clearParam: function (){
			base.options.param = {};
		},

		pressEvent: function (e){
			var base = this;
			var $T = base.options;
			$T.page++;
			base.append();
			if (e) e.preventDefault();
			return false;
		},

		append: function (){
			var base = this;
			var $T = base.options;
			$T.param.page = $T.page;
			$T.ajax = $.ajax({
				url : $T.target.attr("data-url"),
				data : $T.param,
				type : "POST",
				beforeSend: function (xhr){
					$T.isLoading = true;
					$T.target.find(".pager").addClass("loading");
				},
				success: function (data){
					var temp = "";
					$.each($T.param, function (key, val){
						if(temp != "") temp += "&";
						var enc_val = encodeURIComponent(val);
						enc_val = enc_val.replace(/%20/gi, "+");
						temp += key + "=" + enc_val;
					});
					if (temp != this.data) return;

					if($T.target)
					{
						if ($T.target.find(".paging_list").data("hasPager"))
							$(".pager").before(data);
						else
							$T.target.find(".paging_list").append(data);

						//if (MXB.handle) MXB.handle.set($T.target.find(".paging_list"));
						$T.target.find(".pager").removeClass("loading");

						//MXB.run("pager_appended", data);

						if ($T.target.attr("data-complete-function"))
						{
							try {
								if ($T.target.attr("data-complete-function"))
									window[$T.target.attr("data-complete-function")]();
							}
							catch(ex) {
								console.log("inAppScript." + $T.target.attr("data-complete-function") + "();");
								console.log(ex);
							}
						}

						$T.isLoading = false;
						if ($T.target.find(".paging_list .paging_close").size() > 0)
						{
							$T.target.find(".pager").hide();
							$(window).unbind("scroll");
						}
					}
				}
			});
		}
	};

	$.fn.mxbPager = function( target, options ){
		return this.each(function() {
			if($(this).data("vp-init") === true){
				return false;
			}
			$(this).data("vp-init", true);
			var pager = Object.create( mxbPager );
			pager.init( target, options, this );
			$.data( this, "mxbPager", pager );
		});
	};
	
	$.fn.mxbPager.options = {
		target: null,
		param: {},
		target: undefined,
		scrollmode: false,
		page: 1,
		isLoading: false,
		ajax: undefined
	};
	
	//$mxbPager = $(document).mxbPager();

	if ($(".content_body").find("*[data-paging]").size() > 0){
		$mxbPager = $(document).mxbPager($(".content_body").find("*[data-paging]"));
	}
});

function item_loaded(){
	console.log("로드!!");
}