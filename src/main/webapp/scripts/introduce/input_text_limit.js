$(function() {
    var one = 1000;
    var two = 1500;
    var three = 1000;
    function updateInputCount() {
        var oneLength = $('.area-one').val().length;
        var one_count = one - oneLength;
        
        var twoLength = $('.area-two').val().length;
        var two_count = two - twoLength;
        
        var threeLength = $('.area-three').val().length;
        var three_count = three - threeLength;
        
        $('span.counter-one').text(one_count);
        if (one_count < 0) {
            $('span.counter-one').addClass('disabled');
            $('input#input-submit').prop('disabled', true);
        } else {
            $('span.counter-one').removeClass('disabled');
            $('input#input-submit').prop('disabled', false);
        }
        
        $('span.counter-two').text(two_count);
        if (two_count < 0) {
            $('span.counter-two').addClass('disabled');
            $('input#input-submit').prop('disabled', true);
        } else {
            $('span.counter-two').removeClass('disabled');
            $('input#input-submit').prop('disabled', false);
        }
        
        $('span.counter-three').text(three_count);
        if (three_count < 0) {
            $('span.counter-three').addClass('disabled');
            $('input#input-submit').prop('disabled', true);
        } else {
            $('span.counter-three').removeClass('disabled');
            $('input#input-submit').prop('disabled', false);
        }
    }

    $('textarea')
        .focus(updateInputCount)
        .blur(updateInputCount)
        .keypress(updateInputCount);
    window.setInterval(updateInputCount, 100);

    updateInputCount();
});