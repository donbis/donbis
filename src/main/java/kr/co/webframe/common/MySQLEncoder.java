package kr.co.webframe.common;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.authentication.encoding.PasswordEncoder;

@SuppressWarnings("deprecation")
public class MySQLEncoder implements PasswordEncoder {

	@Override
	public String encodePassword(String arg0, Object arg1) {
		return Hasher.parseMySQLPassword(arg0).toUpperCase();
	}

	@Override
	public boolean isPasswordValid(String arg0, String arg1, Object arg2) {

		List<String> passwords = new ArrayList<String>();
		passwords.add(Hasher.parseMySQLPassword(arg1));
		passwords.add(Hasher.parseMySQLPassword(arg1).toUpperCase());
		passwords.add(Hasher.parseMySQLOldPassword(arg1));
		passwords.add(Hasher.parseMySQLOldPassword(arg1).toUpperCase());
		passwords.add(Hasher.parseMySQLNewPassword(arg1));
		passwords.add(Hasher.parseMySQLNewPassword(arg1).toUpperCase());
		passwords.add(Hasher.parseMD5(arg1));
		passwords.add(Hasher.parseMD5(arg1).toUpperCase());
		
		return passwords.contains(arg0);
	}

	
	
}
