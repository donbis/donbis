package kr.co.webframe.common;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class StringUtils {
	
	public static boolean isEmpty(String str) {
		return str == null || str.isEmpty();
	}

	public static String replaceCrLf(String str) {
		return str.replaceAll("\\n", "<br />");
	}

	public static String cutString(String str, Integer len) {
		
		if (str == null || str.isEmpty() || str.length() < len) return str;
		
		String r_val = str.replace("\u00a0", " ");
		int nLength = len;
		int oL = 0, rF = 0, rL = 0;
		
		try {
			
			byte[] bytes = r_val.getBytes("UTF-8"); // 바이트로 보관
			// x부터 y길이만큼 잘라낸다. 한글안깨지게.
			int j = rF;
			
			while (j < bytes.length) {
				if ((bytes[j] & 0x80) != 0) {
					if (oL + 2 > nLength) break;
					
					oL += 2;
					rL += 3;
					j += 3;
				}
				else {
					if (oL + 1 > nLength) break;
					++oL;
					++rL;
					++j;
				}
			}
			
			r_val = new String(bytes, rF, rL, "UTF-8"); // charset 옵션
			return r_val + (str.equals(r_val) ? "" : "..");
			
		}
		catch (Exception e) { }
		return r_val;
	}
	
	public static String urlDecode(String str) {
		if (str == null) return "";
		try {
			return URLDecoder.decode(str, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			return str;
		}
	}
}
