package kr.co.webframe.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.co.webframe.common.PropertiesManager;
import kr.co.webframe.vo.SignedDetails;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class RequestInterceptor extends HandlerInterceptorAdapter {
	
	@Autowired private PropertiesManager pm;

	private List<String> excludePathes = new ArrayList<String>();
	private Map<String, String> excludeParams = new HashMap<String, String>();

	public List<String> getExcludePathes() {
		return excludePathes;
	}

	public void setExcludePathes(List<String> excludePathes) {
		this.excludePathes = excludePathes;
	}
	
	public Map<String, String> getExcludeParams() {
		return excludeParams;
	}

	public void setExcludeParams(Map<String, String> excludeParams) {
		this.excludeParams = excludeParams;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		
		request.setAttribute("http_url", pm.get("website.http_url"));
		request.setAttribute("https_url", pm.get("website.https_url"));
		
		request.setAttribute("_cpx", request.getContextPath());
		request.setAttribute("_agent", request.getHeader("user-agent"));

		UAgentInfo uaInfo = new UAgentInfo(request.getHeader("user-agent"), null);
		boolean isMobile = uaInfo.isMobilePhone;
		
		request.setAttribute("isMobile", isMobile);

		if (request.getPathInfo().lastIndexOf(".ajax") >= 0) return super.preHandle(request, response, handler);
		if (request.getPathInfo().lastIndexOf(".fb") >= 0) return super.preHandle(request, response, handler);
		
		boolean isExclude = false;
		if (excludePathes != null) {
			for(String excludePath : excludePathes) {
				String mappingRegexp = StringUtils.replace(excludePath, "*", ".*");
				if (request.getPathInfo().matches(mappingRegexp)) {
					isExclude = true;
					break;
				}
			}
		}
		
		if (excludeParams != null) {
			for(Map.Entry<String, String> excludeParam : excludeParams.entrySet()) {
				String mappingRegexp = StringUtils.replace(excludeParam.getValue(), "*", ".*");
				if (request.getParameter(excludeParam.getKey()) != null &&
						request.getParameter(excludeParam.getKey()).matches(mappingRegexp)) {
					isExclude = true;
					break;
				}
			}
		}
		if (isExclude) return super.preHandle(request, response, handler);

		Authentication authAccount = ((SecurityContext) SecurityContextHolder.getContext()).getAuthentication();
		
		if(authAccount.getPrincipal() instanceof SignedDetails) {
			request.setAttribute("user", (SignedDetails)authAccount.getPrincipal());
			request.setAttribute("user_id", ((SignedDetails)authAccount.getPrincipal()).getAccountId());
		}
		
		request.setCharacterEncoding("UTF-8");
		
		
		/*if(!request.getMethod().equals("GET")) 
			return super.preHandle(request, response, handler);
		
		if (context != null && context.contains("application/mxb-xhtml")) {
			request.setAttribute("layout", "shared/layout.blank.vm");
			return super.preHandle(request, response, handler); //프레임 씨워져서 왔으면 조건 제외
		}

		if(!isMobile){
			response.addHeader("Access-Control-Allow-Origin", "*");
			response.setHeader("Access-Control-Allow-Headers", "origin, x-requested-with, content-type, accept");
			response.setStatus(301);
			
			response.setHeader("Location", request.getContextPath() + "/#" + request.getContextPath() + request.getPathInfo());
			response.setHeader("Cache-Control", "no-store");
			response.setHeader("Expires", "0");
			return false;
		}
		else {
			request.setAttribute("layout", "./shared/layout.admin.vm");
			return super.preHandle(request, response, handler);
		}*/
		return super.preHandle(request, response, handler);
		
	}
}
