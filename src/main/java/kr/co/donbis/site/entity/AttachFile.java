package kr.co.donbis.site.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "attach_files")
public class AttachFile implements Serializable {

	private long id;
	private String servPath;
	private String filename;
	private String extension;
	private String contentType;
	private long byteLength;
	private String sourceUrl;
	private String metadata;
	private Date createdAt;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "serv_path", nullable = false)
	public String getServPath() {
		return servPath;
	}

	public void setServPath(String servPath) {
		this.servPath = servPath;
	}

	@Column(name = "filename", nullable = false)
	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	@Column(name = "extension", nullable = false)
	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	@Column(name = "content_type", nullable = false)
	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	@Column(name = "byte_length", nullable = false)
	public long getByteLength() {
		return byteLength;
	}

	public void setByteLength(long readBytes) {
		this.byteLength = readBytes;
	}

	@Column(name = "source_url", nullable = true)
	public String getSourceUrl() {
		return sourceUrl;
	}

	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}

	@Column(name = "metadata", nullable = true)
	public String getMetadata() {
		return metadata;
	}

	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	@Column(name = "created_at", nullable = false)
	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

}
