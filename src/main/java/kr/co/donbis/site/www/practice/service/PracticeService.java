package kr.co.donbis.site.www.practice.service;

import kr.co.donbis.site.entity.Introduce;

public interface PracticeService {

	Introduce createIntroduce(String introduce);
	Introduce modifyIntroduce(long id, String introduce);
	
	Introduce findIntroById(long id);
}
