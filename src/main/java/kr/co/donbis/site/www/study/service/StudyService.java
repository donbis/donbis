package kr.co.donbis.site.www.study.service;

import kr.co.donbis.site.entity.BoardCategory;
import kr.co.donbis.site.entity.BoardPost;
import kr.co.donbis.site.www.study.vo.BoardPostCreateVO;
import kr.co.webframe.vo.PagedList;

public interface StudyService {

	BoardPost CreatedBoardPostByMe(BoardPostCreateVO vo, BoardCategory category);
	BoardPost ModifyBoardPostById(long id, BoardPostCreateVO vo);
	BoardPost DeleteBoardPostById(long id);
	BoardCategory findCategoryByPrefix(String category);
	BoardPost findBoardPostById(long id);
	
	
	PagedList findBoardPostByCategoryId(long category, int limit, int page);
	
}
