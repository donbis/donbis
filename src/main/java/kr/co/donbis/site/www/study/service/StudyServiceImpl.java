package kr.co.donbis.site.www.study.service;

import java.util.Date;

import javax.annotation.Resource;

import kr.co.donbis.site.entity.BoardCategory;
import kr.co.donbis.site.entity.BoardPost;
import kr.co.donbis.site.www.study.vo.BoardPostCreateVO;
import kr.co.webframe.vo.PagedList;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

@Service("studyService")
public class StudyServiceImpl implements StudyService {

	@Resource
	private SessionFactory sessionFactory;

	@Override
	public BoardPost CreatedBoardPostByMe(BoardPostCreateVO vo,
			BoardCategory category) {
		Session session = sessionFactory.getCurrentSession();
		session.getTransaction().begin();
		BoardPost board = null;

		try {
			board = new BoardPost();

			board.setCategory_id(category.getId());
			if(vo.getYoutube().startsWith("http")){
				board.setYoutube(vo.getYoutube().substring((int) 31));
			}
			board.setContent(vo.getContent());
			board.setCreated_at(new Date());
			board.setEnabled(true);
			board.setTitle(vo.getTitle());

			session.persist(board);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.getMessage();
			e.printStackTrace();
			session.getTransaction().rollback();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}

		return board;
	}

	@Override
	public BoardCategory findCategoryByPrefix(String category) {
		Session session = sessionFactory.getCurrentSession();
		session.getTransaction().begin();
		BoardCategory cate = null;
		try {
			Criteria cr = session.createCriteria(BoardCategory.class).add(
					Restrictions.eq("prefix", category));
			cate = (BoardCategory) cr.uniqueResult();

			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}

		return cate;

	}

	@Override
	public PagedList findBoardPostByCategoryId(long category, int limit,
			int page) {
		Session session = sessionFactory.getCurrentSession();
		session.getTransaction().begin();

		PagedList messages = new PagedList();

		try {
			Criteria cr = session.createCriteria(BoardPost.class)
					.add(Restrictions.eq("enabled", true))
					.add(Restrictions.eq("category_id", category))
					.setMaxResults(limit);

			cr = cr.setFirstResult(limit * (page - 1)).setMaxResults(limit)
					.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

			messages.setList(cr.list());
			messages.setPage(page);
			messages.setRowSize((Long) cr.setFirstResult(0)
					.setProjection(Projections.rowCount()).uniqueResult());
			messages.setPageRange(page,
					(long) Math.ceil((float) messages.getRowSize() / limit));

			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		return messages;
	}

	@Override
	public BoardPost findBoardPostById(long id) {
		Session session = sessionFactory.getCurrentSession();
		session.getTransaction().begin();

		BoardPost board = null;
		try {
			Criteria cr = session.createCriteria(BoardPost.class).add(
					Restrictions.eq("id", id));

			board = (BoardPost) cr.uniqueResult();

			session.getTransaction().commit();

		} catch (HibernateException e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}

		return board;
	}

	@Override
	public BoardPost ModifyBoardPostById(long id, BoardPostCreateVO vo) {
		Session session = sessionFactory.getCurrentSession();
		session.getTransaction().begin();
		BoardPost modify = null;
		try {
			Criteria cr = session.createCriteria(BoardPost.class).add(
					Restrictions.eq("id", id));

			modify = (BoardPost) cr.uniqueResult();

			modify.setContent(vo.getContent());
			modify.setTitle(vo.getTitle());
			modify.setUpdate_on(new Date());
			if (vo.getYoutube().startsWith("http")) {
				modify.setYoutube(vo.getYoutube().substring((int) 31));
			}
			else{
				modify.setYoutube(vo.getYoutube());
			}
			session.merge(modify);
			session.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		return modify;
	}

	@Override
	public BoardPost DeleteBoardPostById(long id) {
		Session session = sessionFactory.getCurrentSession();
		session.getTransaction().begin();
		BoardPost board = null;
		try {
			Criteria cr = session.createCriteria(BoardPost.class).add(
					Restrictions.eq("id", id));

			board = (BoardPost) cr.uniqueResult();

			board.setEnabled(false);

			session.merge(board);

			session.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		return board;
	}

}
