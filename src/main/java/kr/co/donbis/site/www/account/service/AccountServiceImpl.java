package kr.co.donbis.site.www.account.service;

import javax.annotation.Resource;

import kr.co.donbis.site.entity.Account;
import kr.co.webframe.common.MySQLEncoder;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("accountService")
public class AccountServiceImpl implements AccountService{
	
	@Resource private SessionFactory sessionFactory;
	@Autowired private MySQLEncoder mysqlPasswordEncoder;
	
	@Override
	public Account findAccountById(long id){
		Session session = sessionFactory.getCurrentSession();
		session.getTransaction().begin();
		Account account = null;
		try{Criteria cr = session.createCriteria(Account.class)
				.add(Restrictions.eq("id", id));
			account = (Account)cr.uniqueResult();
			session.getTransaction().commit();			
			
		}
		catch(HibernateException e){
			session.getTransaction().rollback();
			e.printStackTrace();
		}
		return account;
	}
	
	@Override
	public boolean isNotFoundUser(String userId) {
		Session session = sessionFactory.getCurrentSession();
		session.getTransaction().begin();
		try {			
			Criteria criteria = session.createCriteria(Account.class)
									.add(Restrictions.eq("signname", userId))
									.add(Restrictions.eq("enabled", true))
									.setMaxResults(1);
			
			Account account = (Account) criteria.uniqueResult();
			session.getTransaction().commit();
			
			return (account == null);
		} 
		catch (HibernateException e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			return false;
		}
	}
	
	@Override
	public Account updatePasswordByUser(Account account, String vo){
		Session session = sessionFactory.getCurrentSession();
		session.getTransaction().begin();
		Account accounts = null;
		try{
			Criteria cr = session.createCriteria(Account.class)
					.add(Restrictions.eq("id",account.getId()));
			
			accounts = (Account)cr.uniqueResult();
						
			accounts.setPassword(mysqlPasswordEncoder.encodePassword(vo, null));
			
			session.merge(accounts);
			session.getTransaction().commit();
			
		}
		catch(HibernateException e){
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		return accounts;
		
	}
	
/*	@Override
	public PagedList findListByAccount(int page, int limit) {

		PagedList messages = null;
		Session session = sessionFactory.getCurrentSession();
		session.getTransaction().begin();
		
		try {
			Criteria cr = session.createCriteria(Account.class)
					.addOrder(Order.desc("id"));
		
			messages = new PagedList();
			
			cr = cr.setFirstResult(limit * (page - 1)).setMaxResults(limit);
			
			messages.setList(cr.list());
			messages.setPage(page);
			messages.setRowSize((Long) cr.setFirstResult(0).setProjection(Projections.countDistinct("id")).uniqueResult());
			messages.setPageRange(page, (long) Math.ceil((float)messages.getRowSize() / limit));
			
			session.getTransaction().commit();

		} catch (Exception ex) {
			messages = null;
			ex.printStackTrace();
			session.getTransaction().rollback();
		}

		return messages;
	}*/
}
