package kr.co.donbis.site.www.account.service;

import kr.co.donbis.site.entity.Account;

public interface AccountService {

	Account findAccountById(long id);
	boolean isNotFoundUser(String userId);
	
	Account updatePasswordByUser (Account account, String vo);
	
	/*PagedList findListByAccount(int page, int limit);*/
	
	
}
