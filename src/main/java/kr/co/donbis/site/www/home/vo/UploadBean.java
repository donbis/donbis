package kr.co.donbis.site.www.home.vo;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class UploadBean {

	private String fileid;
	private String after;
	private CommonsMultipartFile filedata;

	public String getFileid() {
		return fileid;
	}

	public void setFileid(String fileid) {
		this.fileid = fileid;
	}

	public String getAfter() {
		return after;
	}

	public void setAfter(String after) {
		this.after = after;
	}

	public CommonsMultipartFile getFiledata() {
		return filedata;
	}

	public void setFiledata(CommonsMultipartFile filedata) {
		this.filedata = filedata;
	}

}