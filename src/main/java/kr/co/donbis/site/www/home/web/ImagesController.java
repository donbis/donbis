package kr.co.donbis.site.www.home.web;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletResponse;

import kr.co.donbis.site.entity.AttachFile;
import kr.co.donbis.site.www.home.service.FileService;
import kr.co.donbis.site.www.home.vo.ImageSourceFile;
import kr.co.donbis.site.www.home.vo.UploadBean;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/app_img")
public class ImagesController {

	@Autowired private FileService fileService;
	
	@ResponseBody
	@RequestMapping(value = "/create.ajax", method = RequestMethod.POST)
	public String postfile(UploadBean uploadBean, BindingResult result){
		AttachFile attr = null;
		if(uploadBean.getFiledata().getSize() != 0) attr = fileService.createImageSourceForData(uploadBean.getFiledata());
		return "FILEID:" + attr.getId();
	}

	@RequestMapping(value = "/create.json")
	public ResponseEntity<String> postfileJson(UploadBean uploadBean, BindingResult result){

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "application/x-javascript; charset=UTF-8");
		
		AttachFile attr = null;
		if(uploadBean.getFiledata().getSize() != 0) attr = fileService.createImageSourceForData(uploadBean.getFiledata());
		
		JSONArray array = new JSONArray();
		
		JSONObject file = new JSONObject();
		file.put("id", attr.getId());
		file.put("name", attr.getFilename());
		file.put("size", attr.getByteLength());
		
		array.add(file);
		
		JSONObject document = new JSONObject();
		document.put("success", true);
		document.put("files", array);
		
		String responseBody = document.toString();
		return new ResponseEntity<String>(responseBody, responseHeaders, HttpStatus.OK);
	}

	@RequestMapping(value = "/create.jsx", method = RequestMethod.POST)
	public String postfileJSX(ModelMap model, UploadBean uploadBean, BindingResult result){
		AttachFile attr = null;
		if(uploadBean.getFiledata() != null && uploadBean.getFiledata().getSize() != 0) attr = fileService.createImageSourceForData(uploadBean.getFiledata());
		model.addAttribute("script_content", "parent." + uploadBean.getAfter() + "(" + attr.getId() + ");");
		return "home/script";
	}

	@ResponseBody
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String postfileJSON(UploadBean uploadBean, BindingResult result){
		AttachFile attr = null;
		if(uploadBean.getFiledata().getSize() != 0) attr = fileService.createImageSourceForData(uploadBean.getFiledata());
		return "{'success':true, fileId:'" + attr.getId() + "'}";
	}
	
	@RequestMapping(value = "/get/{image_id}", method = RequestMethod.GET)
	public void filedownload(@PathVariable ("image_id") long imageId, HttpServletResponse response) throws IOException{
		AttachFile imageSource = fileService.getImageSource(imageId);
		ImageSourceFile file = fileService.getSourceFile(imageSource);

		try{
			if (file != null && response != null) {
		        response.setHeader("Content-disposition", (imageSource.getExtension().equals("file") ? "attachment" : "inline") + ";filename=" + URLEncoder.encode(imageSource.getFilename(), "UTF-8"));
		        response.setHeader("Content-Length", String.valueOf(file.getFileLength()));
				FileCopyUtils.copy(file.getInputStream(), response.getOutputStream());
			}
		}
		catch(Exception ex) {
	        response.setHeader("Content-disposition", "inline;filename=" + imageSource.getFilename());
	        response.setHeader("Content-Length", String.valueOf(file.getFileLength()));
	        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}
	}

	@RequestMapping(value = "/thumb/{image_id}", method = RequestMethod.GET)
	public void thumb(@PathVariable ("image_id") long imageId, 
			@RequestParam(value = "w", required = false) long w,
			@RequestParam(value = "h", required = false) long h,
			@RequestParam(value = "t", required = false) String t,
			HttpServletResponse response) throws IOException{
		
		AttachFile imageSource = fileService.getImageSource(imageId);
		ImageSourceFile file = fileService.getSourceFile(imageSource, w, h, t);
		
		try{
			if (file != null && response != null) {
		        response.setHeader("Content-disposition", "inline;filename=" + imageSource.getFilename());
		        response.setHeader("Content-Length", String.valueOf(file.getFileLength()));
				FileCopyUtils.copy(file.getInputStream(), response.getOutputStream());
			}
		}
		catch(Exception ex) {
	        response.setHeader("Content-disposition", "inline;filename=" + imageSource.getFilename());
	        response.setHeader("Content-Length", String.valueOf(file.getFileLength()));
	        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}
	}
	
	
}
