package kr.co.donbis.site.www.practice.web;

import kr.co.donbis.site.entity.Account;
import kr.co.donbis.site.entity.Introduce;
import kr.co.donbis.site.www.account.service.AccountService;
import kr.co.donbis.site.www.practice.service.PracticeService;
import kr.co.webframe.vo.SignedDetails;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/introduce")
public class PracticeController {

	@Autowired private PracticeService practiceService;
	@Autowired private AccountService accountService;
	
	@RequestMapping("")
	public String index(ModelMap model){
		
		Authentication authentication = ((SecurityContext) SecurityContextHolder
				.getContext()).getAuthentication();
		if (!(authentication.getPrincipal() instanceof SignedDetails))
			return "account/login";
		SignedDetails principal = (SignedDetails) authentication.getPrincipal();
		Account account = accountService.findAccountById(principal
				.getAccountId());
		
		long first = 1;
		long second = 2;
		long third = 3;
		
		Introduce one = practiceService.findIntroById(first);
		Introduce two = practiceService.findIntroById(second);
		Introduce three = practiceService.findIntroById(third);
		
		model.addAttribute("account", account);
		model.addAttribute("one", one);
		model.addAttribute("two", two);
		model.addAttribute("three", three);
		
		
		return "introduce/index";
	}
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String intro(@RequestParam(value = "intro-one", required = false) String intro_one,
			@RequestParam(value = "intro-two", required = false) String intro_two,
			@RequestParam(value = "intro-three", required = false) String intro_three){
		
		practiceService.createIntroduce(intro_one);
		practiceService.createIntroduce(intro_two);
		practiceService.createIntroduce(intro_three);
		
		return "redirect:/introduce";
	}
	
	@RequestMapping(value = "/modify/{id}", method = RequestMethod.POST)
	public String modify(
			@PathVariable(value = "id") long id
			,@RequestParam(value = "intro-one", required = false) String intro_one,
			@RequestParam(value = "intro-two", required = false) String intro_two,
			@RequestParam(value = "intro-three", required = false) String intro_three){
		
		practiceService.modifyIntroduce(id, intro_one);
		practiceService.modifyIntroduce(id, intro_two);
		practiceService.modifyIntroduce(id, intro_three);
		
		return "redirect:/introduce";
	}
	
	@RequestMapping("/sub")
	public String submenu(ModelMap model){
		
		Authentication authentication = ((SecurityContext) SecurityContextHolder
				.getContext()).getAuthentication();
		if (!(authentication.getPrincipal() instanceof SignedDetails))
			return "account/login";
		
		SignedDetails principal = (SignedDetails) authentication.getPrincipal();
		Account account = accountService.findAccountById(principal
				.getAccountId());
		
		long first = 4;
		long second = 5;
		long third = 6;
		
		Introduce one = practiceService.findIntroById(first);
		Introduce two = practiceService.findIntroById(second);
		Introduce three = practiceService.findIntroById(third);
		
		model.addAttribute("account", account);
		model.addAttribute("one", one);
		model.addAttribute("two", two);
		model.addAttribute("three", three);
		
		return "introduce/sub";
	}
	@RequestMapping(value = "/sub/{id}", method = RequestMethod.POST)
	public String sub(@PathVariable(value = "id") long id,
			@RequestParam(value = "intro-one", required = false) String intro_one,
			@RequestParam(value = "intro-two", required = false) String intro_two,
			@RequestParam(value = "intro-three", required = false) String intro_three){
		
		practiceService.modifyIntroduce(id, intro_one);
		practiceService.modifyIntroduce(id, intro_two);
		practiceService.modifyIntroduce(id, intro_three);
		
		return "redirect:/introduce/sub";
	}
}
