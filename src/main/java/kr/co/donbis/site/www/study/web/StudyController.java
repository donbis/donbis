package kr.co.donbis.site.www.study.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.co.donbis.site.entity.BoardCategory;
import kr.co.donbis.site.entity.BoardPost;
import kr.co.donbis.site.www.study.service.StudyService;
import kr.co.donbis.site.www.study.vo.BoardPostCreateVO;
import kr.co.webframe.vo.PagedList;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/study")
public class StudyController {
	
	@Autowired private StudyService studyService;
	
	@RequestMapping("")
	public String index(ModelMap model,
			@RequestParam(value = "p", required = false) Integer page){

		if(page == null){
			page = 1;
		}		
		
		int limit = 10;
		long category = 1;
		
		PagedList messages = studyService.findBoardPostByCategoryId(category, limit, page);
		
		model.addAttribute("messages", messages);
		
		return "study/study";
	}
	@RequestMapping("/{id}")
	public String view(ModelMap model,
			@PathVariable("id") long id){
		
		BoardPost board = studyService.findBoardPostById(id);
		
		
		model.addAttribute("content", board);
		
		return "study/view";
	}
	
	@RequestMapping("/write")
	public String write(){
		
		
		
		return "common/write";
	}
	
	@RequestMapping(value = "/write", method = RequestMethod.POST)
	public ResponseEntity<String> write(HttpServletResponse response, HttpServletRequest request, @ModelAttribute ("command")BoardPostCreateVO vo){
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-type", "text/html; charset=UTF-8");
		
		BoardCategory category = studyService.findCategoryByPrefix("study");

		JSONObject responseBody = new JSONObject();
		try{
			studyService.CreatedBoardPostByMe(vo, category);
			responseBody.put("success", true);
			responseBody.put("redirect_url", request.getContextPath()+"/study");
		}
		catch(Exception e){
			responseBody.put("success", false);
			responseBody.put("message",e.getMessage());
		}
		return new ResponseEntity<>(responseBody.toString(), responseHeaders, HttpStatus.OK);
	}
	
	@RequestMapping("/modify/{id}")
	public String modify(ModelMap model,
			@RequestParam(value = "m", required = false) String m,
			@PathVariable("id") long id){
		
		BoardPost board = studyService.findBoardPostById(id);
		
		model.addAttribute("board", board);
		
		return "common/modify";
	}
	@RequestMapping(value = "/modify/{id}", method = RequestMethod.POST)
	public ResponseEntity<String> modify(@PathVariable("id") long id, HttpServletResponse response, HttpServletRequest request, @ModelAttribute ("command")BoardPostCreateVO vo){
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-type", "text/html; charset=UTF-8");
			
		JSONObject responseBody = new JSONObject();
		try{
			studyService.ModifyBoardPostById(id, vo);
			responseBody.put("success", true);
			responseBody.put("redirect_url", request.getContextPath()+"/study/"+id);
		}
		catch(Exception e){
			responseBody.put("success", false);
			responseBody.put("message",e.getMessage());
		}
		return new ResponseEntity<>(responseBody.toString(), responseHeaders, HttpStatus.OK);
	}
	
	@RequestMapping("/delete/{id}")
	public ResponseEntity<String> delete(@PathVariable("id") long id, HttpServletResponse response, HttpServletRequest request){
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-type", "text/html; charset=UTF-8");
			
		JSONObject responseBody = new JSONObject();
		try{
			studyService.DeleteBoardPostById(id);
			responseBody.put("success", true);
			responseBody.put("redirect_url", request.getContextPath()+"/study");
		}
		catch(Exception e){
			responseBody.put("success", false);
			responseBody.put("message",e.getMessage());
		}
		return new ResponseEntity<>(responseBody.toString(), responseHeaders, HttpStatus.OK);
	}
}
