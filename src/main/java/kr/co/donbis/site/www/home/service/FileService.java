package kr.co.donbis.site.www.home.service;

import java.io.File;

import kr.co.donbis.site.entity.AttachFile;
import kr.co.donbis.site.www.home.vo.ImageSourceFile;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public interface FileService {
	
	AttachFile createImageSourceForData(CommonsMultipartFile filedata);
	AttachFile createImageSourceForURL(String imageUrl);
	AttachFile createImageSourceForFile(File url);
	AttachFile getImageSource (long imageId);
	
	ImageSourceFile getSourceFile(AttachFile imageSource);
	ImageSourceFile getSourceFile(AttachFile imageSource, Long width, Long height, String thumbnail_type);


}
