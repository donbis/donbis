package kr.co.donbis.site.www.account.service;

import java.util.ArrayList;
import java.util.Collection;

import kr.co.donbis.site.entity.Account;
import kr.co.webframe.vo.SignedDetails;
import net.sf.json.JSONObject;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@SuppressWarnings("deprecation")
public class MemberDetailsServiceImpl implements UserDetailsService {

	@Autowired private SessionFactory sessionFactory;
	@Autowired private AccountService accountService;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
		
		Session session = sessionFactory.getCurrentSession();
		session.getTransaction().begin();
		
		Account account = null;
		
		try {
			
			Criteria criteria = session.createCriteria(Account.class)
									.add(Restrictions.eq("signname", userId.toLowerCase()))
									.add(Restrictions.eq("enabled", true))
									.setMaxResults(1);

			account = (Account) criteria.uniqueResult();
			session.getTransaction().commit();
		}
		catch(HibernateException ex) {
			session.getTransaction().rollback();
			ex.printStackTrace();
		}

		if (account == null) throw new UsernameNotFoundException("user not found");		
		
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new GrantedAuthorityImpl("ROLE_USER"));
		for(String role : account.getRole().split("\\|")) {
			authorities.add(new GrantedAuthorityImpl(role));
		}

		JSONObject userData = new JSONObject();
		userData.put("user_id", account.getSignname());
		userData.put("user_pk", account.getId());
				
		SignedDetails user = new SignedDetails(account.getSignname(), account.getPassword(), authorities, userData);
		return user;
		
	}

}
