package kr.co.donbis.site.www.home.web;

import kr.co.donbis.site.www.account.service.AccountService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("")
public class HomeController {
	
	@Autowired private AccountService accountService;
	
	@RequestMapping("")
	public String index(ModelMap model,@RequestParam(value = "p", required = false)Integer page){
		if(page == null) page = 1;
			
		return "home/index";
	}
	
	@RequestMapping("/login")
	public String login(){
		
		return "account/login";
	}
	
	@RequestMapping("/logout")
	public String logout(){
		
		return "account/logout";
	}
}
