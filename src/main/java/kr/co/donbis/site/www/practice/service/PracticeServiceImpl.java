package kr.co.donbis.site.www.practice.service;

import java.util.Date;

import javax.annotation.Resource;

import kr.co.donbis.site.entity.Introduce;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

@Service("practiceService")
public class PracticeServiceImpl implements PracticeService{

	@Resource private SessionFactory sessionFactory;
	
	@Override
	public Introduce createIntroduce(String introduce){
	Session session = sessionFactory.getCurrentSession();
	session.getTransaction().begin();
	Introduce intro = null;
		try{
			intro = new Introduce();
			
			intro.setContent(introduce);
			intro.setCreated_at(new Date());
			intro.setEnabled(true);
			
			session.persist(intro);
			session.getTransaction().commit();
		}
		catch(Exception e){
			e.printStackTrace();
			session.getTransaction().rollback();
		}		
		return intro;
	}
	
	@Override
	public Introduce modifyIntroduce(long id, String introduce){
		Session session = sessionFactory.getCurrentSession();
		session.getTransaction().begin();
		Introduce intro = null;
		try{
			Criteria cr = session.createCriteria(Introduce.class)
					.add(Restrictions.eq("id", id));
			
			intro = (Introduce) cr.uniqueResult();
			
			intro.setContent(introduce);
			intro.setUpdate_on(new Date());
			
			session.merge(intro);
			session.getTransaction().commit();
			
		}
		catch(Exception e){
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		
		return intro;
	}
	
	
	@Override
	public Introduce findIntroById(long id){
		Session session = sessionFactory.getCurrentSession();
		session.getTransaction().begin();
		Introduce intro = null;
		try{
			Criteria cr = session.createCriteria(Introduce.class)
					.add(Restrictions.eq("id", id));
			
			intro = (Introduce) cr.uniqueResult();
			
			session.getTransaction().commit();
			
		}
		catch(Exception e){
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		
		return intro;
	}
}
