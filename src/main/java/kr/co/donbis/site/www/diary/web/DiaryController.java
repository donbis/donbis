package kr.co.donbis.site.www.diary.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.co.donbis.site.entity.Account;
import kr.co.donbis.site.entity.BoardCategory;
import kr.co.donbis.site.entity.BoardPost;
import kr.co.donbis.site.www.account.service.AccountService;
import kr.co.donbis.site.www.study.service.StudyService;
import kr.co.donbis.site.www.study.vo.BoardPostCreateVO;
import kr.co.webframe.vo.PagedList;
import kr.co.webframe.vo.SignedDetails;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/diary")
public class DiaryController {

	@Autowired private StudyService studyService;
	@Autowired private AccountService accountService;
	
	@RequestMapping("")
	public String index(ModelMap model,
			@RequestParam(value = "p", required = false) Integer page){

		Authentication authentication = ((SecurityContext) SecurityContextHolder
				.getContext()).getAuthentication();
		if (!(authentication.getPrincipal() instanceof SignedDetails))
			return "account/login";
		
		if(page == null){
			page = 1;
		}		
		
		int limit = 10;
		long category = 2;
		
		PagedList messages = studyService.findBoardPostByCategoryId(category, limit, page);
		
		model.addAttribute("messages", messages);
		return "diary/diary";
	}
	@RequestMapping("/{id}")
	public String view(ModelMap model,
			@PathVariable("id") long id){
		
		BoardPost board = studyService.findBoardPostById(id);
		
		
		model.addAttribute("content", board);
		
		return "diary/view";
	}
	
	@RequestMapping("/write")
	public String write(){
		
		
		
		return "common/write";
	}
	
	@RequestMapping(value = "/write", method = RequestMethod.POST)
	public ResponseEntity<String> write(HttpServletResponse response, HttpServletRequest request, @ModelAttribute ("command")BoardPostCreateVO vo){
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-type", "text/html; charset=UTF-8");
		
		BoardCategory category = studyService.findCategoryByPrefix("diary");

		JSONObject responseBody = new JSONObject();
		try{
			studyService.CreatedBoardPostByMe(vo, category);
			responseBody.put("success", true);
			responseBody.put("redirect_url", request.getContextPath()+"/diary");
		}
		catch(Exception e){
			responseBody.put("success", false);
			responseBody.put("message",e.getMessage());
		}
		return new ResponseEntity<>(responseBody.toString(), responseHeaders, HttpStatus.OK);
	}
}