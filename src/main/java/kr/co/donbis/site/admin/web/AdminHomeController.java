package kr.co.donbis.site.admin.web;

import kr.co.donbis.site.entity.Account;
import kr.co.donbis.site.www.account.service.AccountService;
import kr.co.webframe.common.MySQLEncoder;
import kr.co.webframe.vo.SignedDetails;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/asd")
public class AdminHomeController {
	
	@Autowired private AccountService accountService;
	@Autowired private MySQLEncoder mysql;

	@RequestMapping("")
	public String index(ModelMap model){
		Authentication authentication = ((SecurityContext) SecurityContextHolder
				.getContext()).getAuthentication();
		if (!(authentication.getPrincipal() instanceof SignedDetails))
			return "account/login";
		SignedDetails principal = (SignedDetails) authentication.getPrincipal();
		Account account = accountService.findAccountById(principal
				.getAccountId());
		
		String asd = mysql.encodePassword("sovhs95103", null);
		
		model.addAttribute("asd", asd);
		model.addAttribute("account", account);
		model.addAttribute("layout", "./shared/layout.admin.vm");
		return "admin/index";
	}
}
